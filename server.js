require('dotenv').config()

const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');

// modules
const userRoutes = require('./routes/userRoutes');
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require('./routes/cartRoutes');
const checkoutRoutes = require('./routes/checkoutRoutes');

// express app
const app = express()

// middleware
app.use(cors());
app.use(express());
app.use(express.json())

app.use((req, res, next) => {
  // console.log(req.path, req.method)
  next()
})

// routes
app.use('/api/user', userRoutes);
app.use('/api/products', cartRoutes);
app.use('/api/products', productRoutes);
app.use('/api/products', checkoutRoutes);

// connect to db
mongoose.set("strictQuery", true);
mongoose.connect(process.env.MONGO_URI)
  .then(() => {
    // listen for requests
    app.listen(process.env.PORT || 5000, () => {
      console.log('Connected to database & listening on port', process.env.PORT || 5000)
    })
  })
  .catch((error) => {
    console.log(error)
  })