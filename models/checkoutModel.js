const mongoose = require('mongoose');

const checkoutSchema = new mongoose.Schema(
		{
			cartItemId: { type: String, required: true },
			userId: { type: String, required: true },
			productId: { type: String, required: true },
			title: { type: String },
			images: { type: String },
			quantity: { type: Number, required: true },
			amount: { type: Number, required: true },
			status: {type: String, default: "checked out"}
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("Checkout", checkoutSchema);