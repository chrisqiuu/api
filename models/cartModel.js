const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema(
		{
			userId: { type: String, required: true },
			productId: { type: String, required: true },
			title: { type: String },
			images: { type: String },
			quantity: { type: Number, default: 1},
			amount: { type: Number, required: true },
			status: {type: String, default: "in cart"}
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("Cart", cartSchema);