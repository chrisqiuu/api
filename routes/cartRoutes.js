// dependencies
const router = require('express').Router();

// modules
const cartController = require('../controllers/cartController');
const auth = require('../middleware/auth');

// USER: retrieve cart
router.get('/cart', auth.verify, cartController.getUserCart);

// add to cart
router.post("/addtocart/:productId", auth.verify, cartController.addToCart);

// remove from cart
router.delete("/removefromcart/:cartItemId", auth.verify, cartController.removeFromCart);



module.exports = router;