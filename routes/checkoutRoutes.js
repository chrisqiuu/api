// dependencies
const router = require('express').Router();

// modules
const checkoutController = require('../controllers/checkoutController');
const auth = require('../middleware/auth');

// checkout
router.post("/checkout/:cartItemId", auth.verify, checkoutController.checkout);

module.exports = router;