// dependencies
const router = require("express").Router();

// modules
const userController = require("../controllers/userController");
const auth = require('../middleware/auth');

//// ROUTE HANDLERS

// ALL: register
router.post('/register', userController.checkUsernameExists, userController.checkEmailExists, userController.registerUser);

// ALL: login
router.post('/login', userController.loginUser);

// ALL: get profile details
router.get('/profile', userController.getProfileDetails);

// ADMIN: update user role
router.patch('/updateuserrole/:userId', auth.verify, userController.updateUserRole);


module.exports = router;