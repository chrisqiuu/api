// dependencies
const bcrypt = require('bcrypt');

// import modules
const Users = require('../models/userMod');
const auth = require('../middleware/auth');

// username
const checkUsernameExists = async (request, response, next) => {
	await Users.find({username: request.body.username})
	.then(result => {
		let usernameExists;
		if(result.length > 0) {
			return response.send({usernameExists: true});
		}

		else next();
	})
}

// email
const checkEmailExists = async (request, response, next) => {
	await Users.find({email: request.body.email})
	.then(result => {
		let emailExists;

		if(result.length > 0) {
			return response.send({emailExists: true});
		}
			
		else next();
	})
}

// register
const registerUser = async (request, response) => {
	let newUser = new Users(
			{
				name: request.body.name,
				username: request.body.username,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
			}
		)
	
	let registered;

	await newUser
	.save()
	.then(user => {
		console.log(user);
		response.send({registered: true});
	})
	.catch(error => {
		console.log(error);
		response.send({registered: false});
	})
}

// login
const loginUser = (request, response) => {
	return Users.findOne({username: request.body.username})
	.then(result => {
		console.log(result);

		if(result === null){
			return response.send({accessToken: 'empty'});
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				console.log(token);
				return response.send({accessToken: token});
			}
			else{
				return response.send({accessToken: 'empty'});
			}
		}
	})
}

const getProfileDetails = (request, response) => {
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	

	return Users.findById(userData.id).then(result => {
		result.password = "Confindential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
}

// modify User role
const updateUserRole = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let userId = request.params.userId;

	if(userData.isAdmin) {
		return Users.findById(userId)
		.then(result => {
			let update = {
				isAdmin : !result.isAdmin
			}

			return Users.findByIdAndUpdate(userId, update, {new: true})
			.then(document => {
				document.password = "-------";
				// console.log(document);
				response.send(document)
			})
			.catch(err => response.send(err));
		})
		.catch(err => response.send(err));
	} else response.send(`Access denied!`);
}

module.exports = {
    checkUsernameExists,
    checkEmailExists,
    registerUser,
    loginUser,
	updateUserRole,
	getProfileDetails
}