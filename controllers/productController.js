// import modules
const Products = require('../models/productModel');
const auth= require('../middleware/auth');

// Create a product 
const createProduct = async (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    
    try {
        const {title, price, description, images, category, stock} = request.body;
        let isUserAdmin;

        if(userData.isAdmin) {
            // must contain image
            let hasImage;
            if (images === "") {
                return response.status(400).send({hasImage: false});
            }

            const newProduct = new Products(
                {
                    title: title.toLowerCase(),
                    price,
                    description,
                    images,
                    category,
                    stock
                }
            )

            // check if product exists in shop
            const product = await Products.findOne({ title: request.body.title });
            let productExists;
            if (product)
            return response.status(400).send({ productExists: true });

            let productCreated;
            await newProduct.save();
            response.send({ productCreated: true }); 
        } else {
            return response.status(400).send({ isUserAdmin: false });
        }
    } catch (err) {
        console.log(err);
        return response.status(500).send(false);
    }
}

// NON-ADMIN: Retrieve All Active Products
const getAllActiveProducts = (request, response) => {
	return Products.find({isActive : true})
	.then(result => {
        let activeProductsRender;
		console.log(`Rendering available products.`)
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// ADMIN: Retrieve All Products
const getAllProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin) {
        let isUserAdmin;
		return response.send({ isUserAdmin: false });
	} else {
        let allProductsRender;
		return Products.find({})
		.then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	}
}

// Retrieve Single Product
const getSingleProduct = (request, response) => {
	const productId = request.params.productId;

	return Products.findById(productId).then(result => {
		console.log(`Rendering ${result.title}`)
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// Update a product
const updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	let updatedProduct = {
		title: request.body.title,
		description: request.body.description,
		price: request.body.price,
		stock: request.body.stock,
        category: request.body.category,
        images: request.body.images
	}

	const productId = request.params.productId;
	if(userData.isAdmin) {
		return Products.findByIdAndUpdate(productId, updatedProduct, {new:true})
		.then(result => {
			response.send(result);
		}).catch(err => {
			response.send(err);
		})
	} else {
		return response.send("Access denied.");
	}
}

// Archive / Unarchive Product
const archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	const subjectProduct = Products.findById(productId)
	.then(subjectProduct => {
		let archivedProduct = {
			isActive: !subjectProduct.isActive
		}

		if (userData.isAdmin) {
			return Products.findByIdAndUpdate(productId, archivedProduct, {new:true})
			.then(result => {
				console.log(result);
                let archived;
				if(!result.isActive) {
                    return response.send({archived: true})
                } else {
                    return response.send({archived: false})
                }
			}).catch(err =>{
				response.send(err);
			})
		} else {
			response.send('Access denied.')
		}
	}).catch(err =>{
		response.send(err);
	});
}

module.exports = {
	createProduct,
    getAllActiveProducts,
    getAllProducts,
    getSingleProduct,
    updateProduct,
    archiveProduct,
}