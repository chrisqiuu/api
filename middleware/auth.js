// dependencies
const jwt = require('jsonwebtoken');

// token
const createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, process.env.SECRET, {})
}

// verification
const verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (token !== undefined) {
		token = token.slice(7, token.length);
		// console.log(token);

		return jwt.verify(token, process.env.SECRET, (error) => {
			let isTokenValid;
			if (error) response.send({isTokenValid: false});
			else next();
		})
	}
	else {
		let isTokenProvided;
		return response.send({isTokenProvided: false});
	}
}

// decrypt
const decode = (token) => {
	if (token === undefined) {
		return null;
	}

	else {
		token = token.slice(7, token.length);
		return jwt.verify(token, process.env.SECRET, (error) => {

			if (error) {
				return null
			}
			else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
}

module.exports = {
	createAccessToken,
	verify,
	decode
}